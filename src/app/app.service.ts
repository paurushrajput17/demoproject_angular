import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AppProvider } from './app.provider';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class AppService {
    showSpinner() {
        throw new Error('Method not implemented.');
    }

    httpOptions: any;
    baseUrl = "http://localhost:3001/api/v1/";
    evnvor = 'prod'

    constructor(
        public http: HttpClient,
        private toastr: ToastrService,
        private appProvider: AppProvider,
        private cookieService: CookieService
    ) {
        if (window.location.protocol == 'http:') {
            this.baseUrl = "http://localhost:3001/api/v1/";
        }
    }

    //<==============================error ======================================>
    
    error(response_message: any): any {
        throw new Error("Method not implemented.");
    }

    //<===============================get api=====================================>

    getApi(url: any): Observable<any> {
        this.httpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
            }),
        }
        return this.http.get(this.baseUrl + url, this.httpOptions);
    }

    //<==============================get api with token===============================>

    getApiWithAuth(url: any): Observable<any> {
        // let token = localStorage.getItem('jwtToken');
       let token = this.cookieService.get('jwtToken');
        this.httpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                'Authorization': token ? token : ''
            }),
        }
        return this.http.get(this.baseUrl + url, this.httpOptions);
    }

    //<==================================post api===========================================>

    postApi(url: any, data: any, isHeader: any): Observable<any> {
        if (!isHeader) {
            this.httpOptions = {
                headers: new HttpHeaders({ "Content-Type": "application/json" }),
            }
            return this.http.post(this.baseUrl + url, data);
        }
        else {
            let token = localStorage.getItem('jwtToken');

            this.httpOptions = {
                headers: new HttpHeaders({ "Content-Type": "application/json", 'Authorization': token ? token : '' }),
            }
            return this.http.post(this.baseUrl + url, data, this.httpOptions);
        }
    }

    //<====================================post api with token================================>

    postApiWithAuth(url: any, data: any, isHeader: any): Observable<any> {
        if (!isHeader) {
            this.httpOptions = {
                headers: new HttpHeaders({ "Content-Type": "application/json" }),
            }
        } else {

            // let token = localStorage.getItem('jwtToken');
            let token = this.cookieService.get('jwtToken');
            this.httpOptions = {
                headers: new HttpHeaders({
                    "Content-Type": "application/json",
                    'Authorization': token ? token : ''
                })
            }
        }
        return this.http.post(this.baseUrl + url, data, this.httpOptions);
    }

    //<====================================remember me ===================================================>

    IsRememberMe(rmCheck: any, model: any) {
        if (rmCheck.checked == false) {
            console.log("*****")
        } else {
            console.log("rmCheck========>", rmCheck.checked);
            this.cookieService.set('emailId', model.email);
            this.cookieService.set('password', model.password);
        }
    }

    //<=================================token====================================================>

    setToken(token: string) {
        localStorage.setItem('jwtToken', token);
    }

    getToken() {
        return localStorage.getItem('jwtToken');
    }

    removeToken() {
        return localStorage.removeItem('jwtToken');
    }

    clearLocalStorage() {
        return localStorage.clear();
    }

    //<========================================toastr==============================================>

    success(msg: any) {
        this.toastr.success(msg);
    }

    err(msg: any) {
        this.toastr.error(msg);
    }

}
