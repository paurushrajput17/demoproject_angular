import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from "../app.service"
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup
  formInvalid: any = false
  submitted: any = false
  rmCheck: any
  data = {
    email: '',
    password: '',
  }
  cookieData: any

  constructor(
    private route: Router,
    private service: AppService,
    private fb: FormBuilder,
    private cookieService: CookieService
  ) {

    this.loginForm = this.fb.group({
      emailId: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/)]],
      password: ['', [Validators.required]]
    })

  }

  ngOnInit(): void {
    this.data.email = this.cookieService.get('emailId');
    this.data.password = this.cookieService.get('password');
  }

  onLogIn() {
    this.submitted = true
    if (this.loginForm.invalid) {
      this.formInvalid = true
      return
    }

    this.data = {
      email: this.loginForm.value.emailId,
      password: this.loginForm.value.password,
    }

    this.service.postApi("user/userLogin", this.data, 0).subscribe({
      next: (success: any) => {
        if (success.status == 200) {
          this.service.success(success.message)
          console.log("===============", success.message)
          this.service.setToken(success.data.jwtToken);
          this.cookieService.set('jwtToken', success.data.jwtToken);
          this.rmCheck = document.getElementById("rememberMe");
          this.service.IsRememberMe(this.rmCheck, this.data);
          this.route.navigate(['/dashboard'])
        }
        else {
          console.log("api error")
          this.service.err(success.message)
          setTimeout(() => this.route.navigate(['/']), 2000);
        }
      },
      error: (error: any) => {
        console.log({ error })
      }
    })
  }

}
