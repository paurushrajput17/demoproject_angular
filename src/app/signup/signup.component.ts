import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup
  submitted: any;
  formInvalid: any;
  data: any
  constructor(
    private route: Router,
    private service: AppService,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
  ) {
    this.signUpForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/)]],
      phone: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }

  ngOnInit(): void {
  }

  addUser() {
    this.submitted = true
    if (this.signUpForm.invalid) {
      this.formInvalid = true
      return
    }
    this.data = {
      firstName: this.signUpForm.value.firstName,
      lastName: this.signUpForm.value.lastName,
      email: this.signUpForm.value.email,
      password: this.signUpForm.value.password,
      phone: this.signUpForm.value.phone,
    }
    console.log("======values===========", this.signUpForm.value);

    this.service.postApi("user/userSignup", this.data, 0).subscribe({
      next: (success: any) => {
        if (success.status == 200) {
          this.service.success(success.message)
          this.route.navigate(['/'])
        }
        else {
          console.log("api error")
          this.service.err(success.message)
        }
      },
      error: (error: any) => {
        console.log({ error })
      }
    })
  }
}
