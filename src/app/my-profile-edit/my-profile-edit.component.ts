import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-my-profile-edit',
  templateUrl: './my-profile-edit.component.html',
  styleUrls: ['./my-profile-edit.component.css']
})
export class MyProfileEditComponent implements OnInit {
  profileEdit: FormGroup
  userData: any = []
  data: any;
  submitted: any;
  formInvalid: any;
  constructor(
    private route: Router,
    private service: AppService,
    private fb: FormBuilder,
  ) {
    this.profileEdit = this.fb.group({
      firstName: [''],
      lastName: [''],
      phone: [''],
      password: [''],
    })
  }
  ngOnInit(): void {
    this.getloggerDetails()
  }

  getloggerDetails() {
    this.service.getApiWithAuth("user/userDetails",).subscribe({
      next: (success) => {
        if (success.status == 200) {
          this.userData = success.data
          console.log('======userData======', this.userData)
        } else {
          console.log("api error");
        }
      },
      error: (error) => {
        console.log("error", error);
      }
    })
  }

  editUser() {
    this.submitted = true
    if (this.profileEdit.invalid) {
      this.formInvalid = true
      return
    }
    this.data = {
      firstName: this.profileEdit.value.firstName,
      lastName: this.profileEdit.value.lastName,
      password: this.profileEdit.value.password,
      phone: this.profileEdit.value.phone,
    }
    console.log("======values===========", this.profileEdit.value);

    this.service.postApiWithAuth("user/updateUser", this.data, 1).subscribe({
      next: (success: any) => {
        if (success.status == 200) {
          this.service.success(success.message)
          this.route.navigate(['/myprofile'])
        }
        else {
          console.log("api error")
          this.service.err(success.message)
        }
      },
      error: (error: any) => {
        console.log({ error })
      }
    })
  }
  
}
