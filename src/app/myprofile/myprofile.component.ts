import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

  userData: any = []
  constructor(private route: Router,
    private service: AppService,
    private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.getloggerDetails()
  }

  getloggerDetails() {
    this.service.getApiWithAuth("user/userDetails",).subscribe({
      next: (success) => {
        if (success.status == 200) {
          this.service.success(success.message)
          this.userData = success.data
          console.log('======userData======',this.userData)
        } else {
          this.service.error(success.message)
          console.log("api error");
        }
      },
      error: (error) => {
        console.log("error", error);
      }
    })
  }
}
