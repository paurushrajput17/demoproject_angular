import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SideNabComponent } from './side-nab/side-nab.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { MyProfileEditComponent } from './my-profile-edit/my-profile-edit.component';
import { SignupComponent } from './signup/signup.component';
import { UserGuardService as UserGuard } from '../guard/guard.service';
const routes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: '', component: LoginComponent },
  { path: 'side-nab', component: SideNabComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'dashboard', component: DashboardComponent,canActivate: [UserGuard] },
  { path: 'myprofile', component: MyprofileComponent,canActivate: [UserGuard] },
  { path: 'my-profile-edit', component: MyProfileEditComponent,canActivate: [UserGuard] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
