import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-side-nab',
  templateUrl: './side-nab.component.html',
  styleUrls: ['./side-nab.component.css']
})
export class SideNabComponent implements OnInit {

  constructor(private route: Router,
    private service: AppService,
    private fb: FormBuilder,) { }

  ngOnInit(): void {
  }

  adminLogout() {
    this.service.getApiWithAuth("user/userLogout").subscribe({
      next: (success) => {
        console.log("successss", success);
        if (success.status == 200) {
          this.service.success(success.message)
          this.service.clearLocalStorage();
          // setTimeout(() => this.route.navigate(['/']),2000);
          this.route.navigate(['/'])
        }
        else {
          this.service.success(success.message)
          // window.location.reload()
        }
      },
      error: (error) => {
        console.log({ error })
      }
    })

  }
}
