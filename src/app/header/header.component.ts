import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {  Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userData: any = []
  constructor(private route: Router,
    private service: AppService,
    private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.getloggerDetails()
  }

  adminLogout() {
    this.service.getApiWithAuth("user/userLogout").subscribe({
      next: (success) => {
        console.log("successss", success);
        if (success.status == 200) {
          this.service.success(success.message)
          this.service.clearLocalStorage();
          // setTimeout(() => this.route.navigate(['/']),2000);
          this.route.navigate(['/'])
        }
        else {
          this.service.success(success.message)
          // window.location.reload()
        }
      },
      error: (error) => {
        console.log({ error })
      }
    })

  }

  getloggerDetails() {
    this.service.getApiWithAuth("user/userDetails",).subscribe({
      next: (success) => {
        if (success.status == 200) {
          this.userData = success.data
          
        } else {
          console.log("api error");
        }
      },
      error: (error) => {
        console.log("error", error);
      }
    })
  }
}
