import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root",
})
export class UserGuardService implements CanActivate {
  constructor(
    public router: Router,
    private http: HttpClient
  ) { }
  ngOnInit() { }

  canActivate(): boolean {
    const user = localStorage.getItem('jwtToken');
    if (user) {
      console.log('==========>>>.user', user)
      return true;
    } else {
      this.router.navigateByUrl("/");
      return false;
    }
  }
}
